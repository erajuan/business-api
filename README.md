# APP
Start api
```sh
source venv/bin/activate
export DATABASE_URI='mysql+pymysql://erajuan:toor132@localhost:3306/pymes_dev'
flask run

source venv/bin/activate
export DATABASE_URI='mysql+pymysql://erajuan:toor132@localhost:3306/pymes_dev'
celery -A celery_worker.celery worker --loglevel=info --pool=solo
```
## Setup
```sh
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python manage.py db upgrade
flask run
```
## Produción
```sh
source venv/bin/activate
export DATABASE_URI='mysql+pymysql://erajuan:toor132@localhost:3306/pymes_production'
flask run

source venv/bin/activate
export DATABASE_URI='mysql+pymysql://erajuan:toor132@localhost:3306/pymes_production'
celery -A celery_worker.celery worker --loglevel=info --pool=solo
```
