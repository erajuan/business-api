from app.extensions import celery
from app import create_app
from app.flask_celery import init_celery

# Flask App
app = create_app()
# Worker
init_celery(celery, app)
