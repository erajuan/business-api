#!/usr/bin/env python3
""" CELERY config
"""
from os import getenv
# from celery.schedules import crontab


CONFIG = {
    'CELERY_TIMEZONE': 'UTC',
    'CELERY_BROKER_URL': getenv('CELERY_BROKER_URL', 'redis://localhost:6379'),
    'CELERY_RESULT_BACKEND': getenv('CELERY_RESULT_BACKEND', 'redis://localhost:6379'),

}
