import unittest
import os
import json
from app import create_app

class StatusTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app()
        self.client = self.app.test_client()

    def test_home(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)

if __name__ == "__main__":
    unittest.main(verbosity=2)
