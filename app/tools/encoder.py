""" encode.py
"""
from datetime import datetime

class CustomEncoder(json.JSONEncoder):
    """ CustomEncoder class to Json
    """

    def default(self, obj):
        """ class to dict
        """
        if isinstance(obj, datetime):
            return obj.replace(microsecond=0).isoformat()
        return obj.__dict__
