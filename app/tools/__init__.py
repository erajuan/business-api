""" utils
"""
from random import choice


_LETTERS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-"


def random_key(l=8):
    """ l=lenght
        return Str with (l) lenght
    """
    return "".join(choice(_LETTERS) for i in range(l))

def clear_str(p, lower=False, upper=False):
    """ Clear and transform string
    """
    value = None if p is None else p.strip()
    if p is None:
        return None
    if lower:
        value = value.lower()
    if upper:
        value = value.upper()
    return value
