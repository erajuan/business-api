""" bcrypt.py
"""
import bcrypt
from werkzeug.security import safe_str_cmp


def bcrypt_gen(passwd):
    """ Generate the user passwd
    """
    salt = bcrypt.gensalt(10)
    passwd = passwd.strip().encode('utf-8')
    hashed = bcrypt.hashpw(passwd, salt)
    return hashed


def bcrypt_verify(passwd, hashed):
    """ Checks the password against it's hash to validates the user's password
    """
    if hashed is None:
        return False
    passwd = passwd.strip().encode('utf-8')
    hashed = hashed if isinstance(hashed, bytes) else hashed.encode('utf-8')
    return safe_str_cmp(bcrypt.hashpw(passwd, hashed), hashed)
