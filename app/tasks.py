import logging
import datetime
from werkzeug.security import safe_str_cmp
from flask import current_app
from flask_mail import Message

from app.extensions import celery
from config import Config

from app import mail, create_app


def create_email_data(config, recipient):
    """ create initial email data
        config: Config actual
        recipient: E-mail que recepciona el mensaje
    """
    email_data = {
        'sender': (config.nombre_comercial, config.email_sender),
        'recipients': [recipient]
    }
    if not safe_str_cmp(config.email_sender, config.email):
        email_data['reply_to'] = config.email
    return email_data


def _send_async_smtp(data):
    """ send email
    """
    msg = Message(
        data['subject'],
        sender=data['sender'],
        reply_to=data['reply_to'],
        recipients=data['recipients']
    )
    msg.body = data['body']
    if data['html'] is not None:
        print(data['html'])
        msg.html = data['html']
    mail.send(msg)


@celery.task(name='hi')
def hi(name=None):
    return True


@celery.task(name='send_mail')
@celery.task
def send_mail(email_data):
    _send_async_smtp(email_data)
