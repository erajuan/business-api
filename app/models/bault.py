from datetime import datetime
from sqlalchemy import desc

from app.extensions import db


class Plantilla(db.Model):

    __tablename__ = 'plantillas'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nombre = db.Column(db.String(100), nullable=False)
    descripcion = db.Column(db.String(100), nullable=False)
    active = db.Column(db.Boolean(), nullable=False)
    cid = db.Column(db.Integer(), nullable=False)

    def __init__(self, cid):
        self.cid = cid

    def to_dict(self):
        """ return diccionario
        """
        return {
            'id': self.id,
            'nombre': self.nombre,
            'descripcion': self.descripcion,
        }

    def add(self, not_bulk=True):
        """ add to session
        """
        db.session.add(self)
        if not_bulk:
            db.session.commit()

    def commit(self):
        """ presistencia en base de datos
        """
        db.session.commit()

    @classmethod
    def get(cls, cid, pk):
        """ Find one by primary key
        """
        query = cls.query.get(pk)
        if query is not None and query.cid == cid:
            return query
        return None

    @classmethod
    def all(cls, cid):
        """ buscar todos del config
        """
        return cls.query.filter(cls.cid == cid)


class Doc(db.Model):

    __tablename__ = 'documentos'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nombre = db.Column(db.String(64), nullable=False)
    template_id = db.Column(db.Integer, nullable=True)
    created_at = db.Column(db.DateTime, nullable=False)
    created_by = db.Column(db.String(16), nullable=False)
    shared = db.Column(db.Boolean, nullable=False, default=True)

    def add(self, created_by, not_bulk=True):
        """ add to session
        """
        self.created_by = created_by
        self.created_at = datetime.utcnow()
        db.session.add(self)
        if not_bulk:
            db.session.commit()

    def commit(self):
        """ presistencia en base de datos
        """
        db.session.commit()

    @classmethod
    def get(cls, cid, pk):
        """ Find one by primary key
        """
        query = cls.query.get(pk)
        if query is not None and query.cid == cid:
            return query
        return None

    @classmethod
    def all(cls, cid):
        """ buscar todos del config
        """
        return cls.query.filter(cls.cid == cid).order_by(desc(cls.id))


class Contrato(db.Model):

    __tablename__ = 'contratos'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    correlativo = db.Column(db.Integer, nullable=False)
    nombre = db.Column(db.String(100), nullable=False)
    empresa_id = db.Column(db.Integer, nullable=False)
    plantilla_id = db.Column(db.Integer, nullable=False)
    estado = db.Colmn(db.String(8), nullable=False)
    valid_until = db.Column(db.Date, nullable=True)
    created_at = db.Column(db.DateTime, nullable=False)
    created_by = db.Column(db.String(16), nullable=False)
    shared = db.Column(db.Boolean, nullable=False, default=True)
    version=db.Column(db.Integer(), nullable = False, default = 1)

    active=db.Column(db.Boolean(), nullable = False)
    cid=db.Column(db.Integer(), nullable = False)

    def __init__(self, cid):
        self.cid=cid

    def to_dict(self):
        """ return diccionario
        """
        return {
            'id': self.id,
            'correlativo': self.correlativo,
            'nombre': self.nombre,
            'empresa_id': self.empresa_id,
            'plantilla_id': self.plantilla_id,
            'estado': self.estado,
            'valid_until': None if self.valid_until is None else self.valid_until.isoformat(),
            'created_at': self.created_at.isoformat(),
            'version': self.version,
            'active': self.active
        }

    def add(self, created_by, not_bulk=True):
        """ add to session
        """
        self.created_by = created_by
        self.created_at = datetime.utcnow()
        db.session.add(self)
        if not_bulk:
            db.session.commit()

    def commit(self):
        """ presistencia en base de datos
        """
        db.session.commit()

    @classmethod
    def get(cls, cid, pk):
        """ Find one by primary key
        """
        query = cls.query.get(pk)
        if query is not None and query.cid == cid:
            return query
        return None

    @classmethod
    def all(cls, cid):
        """ buscar todos del config
        """
        return cls.query.filter(cls.cid == cid).order_by(desc(cls.id))
