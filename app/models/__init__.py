from datetime import datetime
from sqlalchemy import desc

from app.extensions import db


class Config(db.Model):

    __tablename__ = 'configs'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    razon_social = db.Column(db.String(100), nullable=False)
    nombre_comercial = db.Column(db.String(100), nullable=False)
    ruc = db.Column(db.String(16), nullable=False)
    email = db.Column(db.String(64), nullable=False)
    email_sender = db.Column(db.String(64), nullable=False)
    telefono = db.Column(db.String(32), nullable=True)
    whatsapp = db.Column(db.String(16), nullable=True)

    enabled = db.Column(db.Boolean(), nullable=False, default=False)
    referral = db.Column(db.String(8), nullable=False)
    active = db.Column(db.Boolean(), nullable=False, default=False)

    def to_dict(self):
        return {
            'razon_social': self.razon_social,
            'nombre_comercial': self.nombre_comercial,
            'ruc': self.ruc,
            'email': self.email,
            'email_sender': self.email_sender,
            'telefono': self.telefono,
            'whatsapp': self.whatsapp,
            'enable': self.enabled,
            'referral': self.referral
        }

    def save(self):
        """ add to session
        """
        db.session.add(self)
        db.session.commit()

    def new(self):
        """ Nuevo
        """
        self.save()

    @ classmethod
    def get(cls, pk):
        """ Find one by primary key
        """
        return cls.query.get(pk)


class User(db.Model):

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(16))
    _password = db.Column(db.String(240), nullable=True)
    nombre = db.Column(db.String(64), nullable=True)
    email = db.Column(db.String(128), nullable=False)
    is_admin = db.Column(db.Boolean, default=False)

    last_login = db.Column(db.DateTime, nullable=True)
    active = db.Column(db.Boolean, default=True)
    created_at = db.Column(db.DateTime, nullable=False)
    cid = db.Column(db.Integer, nullable=False)

    def __init__(self, cid, username):
        self.cid = cid
        self.username = username.strip().upper()

    def set_passwd(self, passwd):
        """ set
        """
        self._password = passwd.strip()

    @property
    def get_passwd(self):
        """ get
        """
        return self._password

    def to_dict(self):
        """ json
        """
        return {
            'id': self.id,
            'username': self.username,
            'nombre': self.nombre,
            'email': self.email,
            'is_admin': self.is_admin,
            'last_login': None if self.last_login is None else self.last_login.isoformat(),
            'active': self.active
        }

    def not_exists(self):
        """ search username
        """
        query = self.query.filter_by(username=self.username).first()
        return True if query is None else False

    def save(self):
        """ add to session
        """
        self.email = self.email.strip()
        db.session.add(self)
        db.session.commit()

    def new(self):
        """ Nuevo
        """
        self.email = self.email.strip()
        self.created_at = datetime.utcnow()
        self.save()

    @ classmethod
    def by_username(cls, username):
        username = username.strip().upper()
        return cls.query.filter_by(username=username).first()

    @ classmethod
    def get(cls, cid, pk):
        """ Find one by primary key
        """
        query = cls.query.get(pk)
        if query is not None and query.cid == cid:
            return query
        return None

    @ classmethod
    def all(cls, cid):
        """ buscar todos del config
        """
        return cls.query.filter(cls.cid == cid).order_by(desc(cls.id))
