from datetime import datetime
from sqlalchemy import desc

from app.extensions import db


class Servicio(db.Model):

    __tablename__ = 'servicios'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nombre = db.Column(db.String(100), nullable=False)
    descripcion = db.Column(db.String(100), nullable=False)
    moneda = db.Column(db.String(3), nullable=False)
    precio = db.Column(db.Float(), nullable=False, default=0)
    unidad_medida = db.Column(db.String(4), nullable=False, default="NUI")
    archivo_id = db.Column(db.Integer(), nullable=True)

    active = db.Column(db.Boolean(), nullable=False)
    cid = db.Column(db.Integer(), nullable=False)

    def __init__(self, cid):
        self.cid = cid

    def to_dict(self):
        """ return diccionario
        """
        return {
            'id': self.id,
            'nombre': self.nombre,
            'descripcion': self.descripcion,
            'moneda': self.moneda,
            'precio': self.precio,
            'unidad_medidad': self.unidad_medida,
            'archivo': self.archivo_id,
            'active': self.active
        }

    def save(self):
        """ add to session
        """
        db.session.add(self)
        db.session.commit()

    def new(self):
        """ Nuevo
        """
        self.save()

    @ classmethod
    def get(cls, cid, pk):
        """ Find one by primary key
        """
        query = cls.query.get(pk)
        if query is not None and query.cid == cid:
            return query
        return None

    @ classmethod
    def all(cls, cid):
        """ buscar todos del config
        """
        return cls.query.filter(cls.cid == cid).order_by(desc(cls.id))


class Empleado(db.Model):

    __tablename__ = 'empleados'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    titulo = db.Column(db.String(16), nullable=True)
    nombre = db.Column(db.String(64), nullable=False)
    apellido = db.Column(db.String(64), nullable=False)
    documento_tipo = db.Column(db.String(3), nullable=False, default='DNI')
    documento_numero = db.Column(db.String(16), nullable=False)
    sexo = db.Column(db.String(1), nullable=False)
    fecha_nacimiento = db.Column(db.Date(), nullable=False)
    email_asignado = db.Column(db.String(64), nullable=True)
    email = db.Column(db.String(64), nullable=True)
    telefono = db.Column(db.String(64), nullable=True)
    direccion = db.Column(db.String(128), nullable=True)
    observacion = db.Column(db.String(240), nullable=True)

    cargo = db.Column(db.String(32), nullable=False)
    fecha_ingreso = db.Column(db.Date(), nullable=True)
    sueldo = db.Column(db.Float(), nullable=False)
    condicion = db.Column(db.String(16), nullable=False)
    sede = db.Column(db.String(16), nullable=False)

    active = db.Column(db.Boolean, nullable=False, default=True)
    cid = db.Column(db.Integer(), nullable=False)

    def __init__(self, cid):
        self.cid = cid

    def to_dict_teaser(self):
        return {
            'id': self.id,
            'titulo': self.titulo,
            'nombre': self.nombre,
            'apellido': self.apellido,
            'documento_tipo': self.documento_tipo,
            'documento_numero': self.documento_numero,
            'sexo': self.sexo,
            'cargo': self.cargo,
            'email_asignado': self.email_asignado,
            'email': self.email,
            'telefono': self.telefono,
            'active': self.active
        }

    def to_dict(self):
        """ obj to dict
        """
        return {
            'id': self.id,
            'titulo': self.titulo,
            'nombre': self.nombre,
            'apellido': self.apellido,
            'documento_tipo': self.documento_tipo,
            'documento_numero': self.documento_numero,
            'sexo': self.sexo,
            'fecha_nacimiento': self.fecha_nacimiento.isoformat(),
            'email_asignado': self.email_asignado,
            'email': self.email,
            'telefono': self.telefono,
            'direccion': self.direccion,
            'observacion': self.observacion,
            'cargo': self.cargo,
            'fecha_ingreso': None if self.fecha_ingreso is None else self.fecha_ingreso.isoformat(),
            'sueldo': self.sueldo,
            'condicion': self.condicion,
            'sede': self.sede,
            'active': self.active
        }

    def save(self):
        """ add to session
        """
        db.session.add(self)
        db.session.commit()

    def new(self):
        """ Nuevo
        """
        self.save()

    @ classmethod
    def get(cls, cid, pk):
        """ Find one by primary key
        """
        query = cls.query.get(pk)
        if query is not None and query.cid == cid:
            return query
        return None

    @ classmethod
    def all(cls, cid):
        """ buscar todos del config
        """
        return cls.query.filter(cls.cid == cid).order_by(desc(cls.id))


class Empresa(db.Model):
    __tablename__ = 'empresas'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nombre = db.Column(db.String(128), nullable=False)
    rubro = db.Column(db.String(32), nullable=False)
    # comment="1: Cliente, 2: Proveedor, 3: Ambos")
    relacion = db.Column(db.Integer(), nullable=False)
    documento_tipo = db.Column(db.String(8), nullable=True)
    documento_numero = db.Column(db.String(16), nullable=True)
    email = db.Column(db.String(128), nullable=True)
    web = db.Column(db.String(64), nullable=True)
    telefono = db.Column(db.String(64), nullable=True)
    direccion = db.Column(db.String(128), nullable=True)
    ciudad = db.Column(db.String(32), nullable=True)
    observacion = db.Column(db.String(200), nullable=True)

    active = db.Column(db.Boolean, default=True)
    cid = db.Column(db.Integer, nullable=False)

    def __init__(self, cid):
        self.cid = cid

    def to_dict_teaser(self):
        """ to small dict
        """
        return {
            'id': self.id,
            'nombre': self.nombre,
            'email': self.email,
            'telefono': self.telefono,
            'direccion': self.direccion,
            'documento_tipo': self.documento_tipo,
            'documento_numero': self.documento_numero
        }

    def to_dict(self):
        """ to_dict
        """
        return {
            'id': self.id,
            'nombre': self.nombre,
            'rubro': self.rubro,
            'relacion': self.relacion,
            'documento_tipo': self.documento_tipo,
            'documento_numero': self.documento_numero,
            'email': self.email,
            'telefono': self.telefono,
            'direccion': self.direccion,
            'ciudad': self.ciudad,
            'observacion': self.observacion,
            'active': self.active
        }

    def save(self):
        """ add to session
        """
        db.session.add(self)
        db.session.commit()

    def new(self):
        """ Nuevo
        """
        self.save()

    @ classmethod
    def get(cls, cid, pk):
        """ Find one by primary key
        """
        query = cls.query.get(pk)
        if query is not None and query.cid == cid:
            return query
        return None

    @ classmethod
    def all(cls, cid):
        """ buscar todos del config
        """
        return cls.query.filter(cls.cid == cid).order_by(desc(cls.id))


class Contacto(db.Model):

    __tablename__ = 'empresas_contactos'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    empresa_id = db.Column(db.Integer, nullable=False)
    titulo = db.Column(db.String(16), nullable=True)
    nombre = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), nullable=True)
    telefono = db.Column(db.String(64), nullable=True)
    direccion = db.Column(db.String(128), nullable=True)
    ciudad = db.Column(db.String(32), nullable=False)
    documento_identidad = db.Column(db.String(32), nullable=True)
    cargo = db.Column(db.String(32), nullable=True)
    observacion = db.Column(db.String(220), nullable=True)

    active = db.Column(db.Boolean, nullable=False, default=True)
    cid = db.Column(db.Integer, nullable=False)

    def __init__(self, cid, empresa_id):
        self.cid = cid
        self.empresa_id = empresa_id

    def to_dict(self):
        """ to dict
        """
        return {
            'id': self.id,
            'empresa_id': self.empresa_id,
            'titulo': self.titulo,
            'nombre': self.nombre,
            'email': self.email,
            'telefono': self.telefono,
            'direccion': self.direccion,
            'ciudad': self.ciudad,
            'documento_identidad': self.documento_identidad,
            'cargo': self.cargo,
            'observacion': self.observacion,
            'active': self.active
        }

    def save(self):
        """ add to session
        """
        db.session.add(self)
        db.session.commit()

    def new(self):
        """ Nuevo
        """
        self.save()

    @ classmethod
    def get(cls, cid, empresa_id, pk):
        """ Find one by primary key
        """
        query = cls.query.get(pk)
        if query is None:
            return None
        if query.cid == cid and query.empresa_id == empresa_id:
            return query
        return None

    @ classmethod
    def all(cls, cid, empresa_id):
        """ buscar todos del config y empresa
        """
        return cls.query.filter(cls.cid == cid, cls.empresa_id == empresa_id)
