from datetime import datetime
from typing import DefaultDict
from sqlalchemy import desc
from sqlalchemy.sql.expression import nullslast

from app.extensions import db


class Cuenta(db.Model):
    """ Cuenta de banco / caja
    """
    __tablename__ = 'cuentas'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nombre = db.Column(db.String(32), nullable=False)
    moneda = db.Column(db.String(3), nullable=False)
    numero = db.Column(db.String(64), nullable=False)
    descripcion = db.Column(db.String(200), nullable=True)
    balance = db.Column(db.Float(), nullable=False, default=0)

    active = db.Column(db.Boolean, nullable=False)
    cid = db.Column(db.Integer(), nullable=False)

    def __init__(self, cid):
        self.cid = cid

    def to_dict(self):
        """ to dict
        """
        return {
            'id': self.id,
            'nombre': self.nombre,
            'moneda': self.moneda,
            'numero': self.numero,
            'descripcion': self.descripcion,
            'balance': self.balance,
            'active': self.active
        }

    def save(self):
        """ add to session
        """
        db.session.add(self)
        db.session.commit()

    def new(self):
        """ Nuevo
        """
        self.created_at = datetime.utcnow()
        self.save()

    @ classmethod
    def get(cls, cid, pk):
        """ Find one by primary key
        """
        query = cls.query.get(pk)
        if query is not None and query.cid == cid:
            return query
        return None

    @ classmethod
    def all(cls, cid):
        """ buscar todos del config
        """
        return cls.query.filter(cls.cid == cid).order_by(desc(cls.id))


class Movimiento(db.Model):
    __tablename__ = 'movimientos'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    cuenta_id = db.Column(db.Integer, nullable=False)
    # ingreso|egreso|cierre|apertura
    tipo = db.Column(db.String(1), nullable=False)
    correlativo = db.Column(db.Integer, nullable=False)
    forma_pago = db.Column(db.String(32), nullable=True)
    empresa_id = db.Column(db.Integer, nullable=True)
    moneda = db.Column(db.String(3), nullable=False)
    monto = db.Column(db.Float(), nullable=False)
    descripcion = db.Column(db.String(240), nullable=True)
    created_at = db.Column(db.DateTime, nullable=False)
    created_by = db.Column(db.String(16), nullable=False)
    cid = db.Column(db.Integer, nullable=False)

    def __init__(self, cid):
        self.cid = cid

    def to_dict(self):
        """ to dict
        """
        return {
            'tipo': self.tipo,
            'correlativo': self.correlativo,
            'forma_pago': self.forma_pago,
            'empresa_id': self.empresa_id,
            'moneda': self.moneda,
            'monto': self.monto,
            'descripcion': self.descripcion,
            'created_at': self.created_at.isoformat(),
            'created_by': self.created_by
        }

    def save(self):
        """ add to session
        """
        db.session.add(self)
        db.session.commit()

    def new(self, created_by):
        """ Nuevo
        """
        self.created_by = created_by
        self.created_at = datetime.utcnow()
        self.save()

    @ classmethod
    def get(cls, cid, pk):
        """ Find one by primary key
        """
        query = cls.query.get(pk)
        if query is not None and query.cid == cid:
            return query
        return None

    @ classmethod
    def all(cls, cid):
        """ buscar todos del config
        """
        return cls.query.filter(cls.cid == cid).order_by(desc(cls.id))


class Orden(db.Model):

    __tablename__ = 'ordenes'

    empresa_id = db.Column(
        db.Integer,
        db.ForeignKey('empresas.id'),
        nullable=False
    )
    empresa = db.relationship(
        'Empresa',
        backref='ordenes_empresas',
        foreign_keys=[empresa_id]
    )
    moneda = db.Column(db.String(3), nullable=False)
    sub_total = db.Column(db.Float(), nullable=False)
    descuento = db.Column(db.Float(), nullable=False, default=0)
    es_renovacion = db.Column(db.Boolean(), nullable=False, default=False)
    observacion = db.Column(db.String(200), nullable=True)
    fecha_vencimiento = db.Column(db.Date(), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    created_by = db.Column(db.String(16), nullable=False)
    contrato_id = db.Column(db.Integer(), nullable=True)
    incluye_igv = db.Column(db.Boolean(), nullable=False)
    # 0: anulado, 1: pendiente 3: pagado
    estado = db.Column(db.Integer(), nullable=False)

    id = db.Column(db.Integer, primary_key=True)
    cid = db.Column(db.Integer, primary_key=True, nullable=False)

    def __init__(self, cid):
        self.cid = cid

    def to_dict(self):
        """ to dict
        """
        return {
            'id': self.id,
            'cid': self.cid,
            'empresa_id': self.empresa_id,
            'empresa': self.empresa.to_dict_teaser(),
            'moneda': self.moneda,
            'sub_total': self.sub_total,
            'descuento': self.descuento,
            'es_renovacion': self.es_renovacion,
            'observacion': self.observacion,
            'fecha_vencimiento': self.fecha_vencimiento.isoformat(),
            'created_at': self.created_at.isoformat(),
            'created_by': self.created_by,
            'contrato_id': self.contrato_id,
            'incluye_igv': self.incluye_igv,
            'estado': self.estado
        }

    def save(self):
        """ add to session
        """
        db.session.add(self)
        db.session.commit()

    def new(self, created_by):
        """ Nuevo
        """
        self.id = Orden.next(self.cid)
        self.created_by = created_by
        self.created_at = datetime.utcnow()
        self.save()

    @ classmethod
    def next(cls, cid):
        query = Orden.query\
            .filter(cls.cid == cid)\
            .order_by(desc(cls.id))\
            .first()
        return 1 if query is None else query.id + 1

    @ classmethod
    def get(cls, cid, pk):
        """ Find one by primary key
        """
        return cls.query.get((pk, cid))

    @ classmethod
    def all(cls, cid):
        """ buscar todos del config
        """
        return cls.query.filter(cls.cid == cid).order_by(desc(cls.id))


class OrdenItem(db.Model):
    __tablename__ = 'ordenes_items'

    id = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(200), nullable=False)
    cantidad = db.Column(db.Float(), nullable=False)
    precio_unitario = db.Column(db.Float(), nullable=False)

    servicio_id = db.Column(db.Integer, nullable=True)
    orden_id = db.Column(db.Integer, nullable=False)
    cid = db.Column(db.Integer, nullable=False)
    active = db.Column(db.Boolean, nullable=False, default=True)

    def __init__(self, cid, orden_id):
        self.cid = cid
        self.orden_id = orden_id

    def to_dict(self):
        return {
            'id': self.id,
            'descripcion': self.descripcion,
            'cantidad': self.cantidad,
            'precio_unitario': self.precio_unitario,
            'servicio_id': self.servicio_id,
            'active': self.active
        }

    def add(self, commit=True):
        """ add to session
        """
        db.session.add(self)
        if commit:
            db.session.commit()

    @ classmethod
    def get(cls, orden_id, pk):
        """ get one
        """
        query = OrdenItem.query.get(pk)
        if query is not None and query.orden_id == orden_id:
            return query
        return None

    @ classmethod
    def commit(cls):
        """ presistencia en base de datos
        """
        db.session.commit()

    @ classmethod
    def all(cls, cid, orden_id):
        """ buscar todos del config
        """
        return cls.query.filter(cls.cid == cid, cls.orden_id == orden_id)

    @ classmethod
    def destroy(cls, cid, orden_id):
        cls.query.filter(cls.cid == cid, cls.orden_id == orden_id).delete()
        db.session.commit()
        return True


class Suscripcion(db.Model):
    """ Cobros recurrentes
    """
    __tablename__ = 'suscripciones'

    moneda = db.Column(db.String(3), nullable=False)
    precio = db.Column(db.Float(), nullable=False)
    descripcion = db.Column(db.String(250), nullable=True)
    contrato_id = db.Column(db.Integer, nullable=True)
    fecha_inicio = db.Column(db.Date, nullable=False)
    fecha_fin = db.Column(db.Date, nullable=False)
    empresa_id = db.Column(
        db.Integer,
        db.ForeignKey('empresas.id'),
        nullable=False
    )
    empresa = db.relationship(
        'Empresa',
        backref='suscripciones_empresas',
        foreign_keys=[empresa_id]
    )

    active = db.Column(db.Boolean, nullable=False)
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    cid = db.Column(db.Integer(), primary_key=True, nullable=False)

    def __init__(self, cid):
        self.cid = cid

    def to_dict(self):
        """ to dict
        """
        return {
            'id': self.id,
            'moneda': self.moneda,
            'precio': self.precio,
            'descripcion': self.descripcion,
            'contrato_id': self.contrato_id,
            'fecha_inicio': self.fecha_inicio.isoformat(),
            'fecha_fin': self.fecha_fin.isoformat(),
            'empresa_id': self.empresa_id,
            'empresa': self.empresa.to_dict_teaser(),
            'active': self.active
        }

    def save(self):
        """ add to session
        """
        db.session.add(self)
        db.session.commit()

    def new(self):
        """ Nuevo
        """
        self.id = Suscripcion.next(self.cid)
        self.created_at = datetime.utcnow()
        self.save()

    @ classmethod
    def next(cls, cid):
        query = cls.query\
            .filter(cls.cid == cid)\
            .order_by(desc(cls.id))\
            .first()
        return 1 if query is None else query.id + 1

    @ classmethod
    def get(cls, cid, pk):
        """ Find one by primary key
        """
        query = cls.query.get((pk, cid))
        return query

    @ classmethod
    def all(cls, cid):
        """ buscar todos del config
        """
        return cls.query.filter(cls.cid == cid).order_by(desc(cls.id))
