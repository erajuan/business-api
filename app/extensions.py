from celery import Celery
from flask_sqlalchemy import SQLAlchemy
from celery_config import CONFIG
from flask_mail import Mail


def make_celery(app_name=__name__):
    """ make celery
    """
    celery = Celery(
        app_name,
        backend=CONFIG["CELERY_RESULT_BACKEND"],
        broker=CONFIG["CELERY_BROKER_URL"]
    )
    return celery

# Tasks
celery = make_celery()
# SQL ORM
db = SQLAlchemy()
# Flask Mail
mail = Mail()
