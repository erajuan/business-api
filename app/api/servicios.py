""" servicios.py
"""
from datetime import datetime
from flask import Blueprint, request, jsonify
from webargs import fields
from webargs.flaskparser import parser

from app.tools import clear_str
from app.models.core import Servicio
from app.api import auth

blueprint = Blueprint('servicios', __name__)


@blueprint.route('', methods=['GET'])
@auth
def find_all(cid, current_user):
    """ filter
    """
    body = list(map(lambda o: o.to_dict(), Servicio.all(cid)))
    return jsonify(body), 200


arg_req = {
    'nombre': fields.Str(required=True),
    'descripcion': fields.Str(allow_none=True),
    'moneda': fields.Str(required=True),
    'precio': fields.Float(required=True),
    'archivo_id': fields.Integer(allow_none=True)
}


@blueprint.route('', methods=['POST'])
@auth
def create(cid, current_user):
    """ create
    """
    arg = parser.parse(arg_req, request, location='json')
    query = Servicio(cid)
    query.nombre = arg['nombre'].strip()
    query.descripcion = clear_str(arg['descripcion'])
    query.moneda = arg['moneda'].strip().upper()
    query.precio = arg['precio']
    query.active = True
    query.new()
    return "", 201


@blueprint.route('<int:pk>', methods=['GET'])
@auth
def find_one(cid, current_user, pk):
    """ get one
    """
    query = Servicio.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not Found'}
    if http_status == 200:
        body = query.to_dict()
    return body, http_status


@blueprint.route('<int:pk>', methods=['PUT'])
@auth
def update(cid, current_user, pk):
    """ update
    """
    arg = parser.parse(arg_req, request, location='json')
    query = Servicio.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not Found'}
    if http_status == 200:
        query.nombre = arg['nombre'].strip()
        query.descripcion = clear_str(arg['descripcion'])
        query.moneda = arg['moneda'].strip().upper()
        query.precio = arg['precio']
        query.active = True
        query.save()
        http_status = 204
        body = {'message': 'Updated'}
    return body, http_status


@blueprint.route('<int:pk>', methods=['DELETE'])
@auth
def delete(cid, current_user, pk):
    """ update
    """
    query = Servicio.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not found'}
    if http_status == 200:
        query.active = False
        query.save()
        http_status = 204
        body = {'message': 'Deleted'}
    return body, http_status
