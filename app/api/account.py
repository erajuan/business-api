""" account
"""
from datetime import datetime
from werkzeug.security import safe_str_cmp
from flask import Blueprint, request
from webargs import fields
from webargs.flaskparser import parser

from app.tools import clear_str
from app.api import auth
from app.api import bearer_encode
from app.tools.bcrypt import bcrypt_gen, bcrypt_verify
from app.tools import random_key
from app.models import User
from app.models import Config

blueprint = Blueprint('account', __name__)


@blueprint.route('', methods=['GET'])
@auth
def current(cid, current_user):
    """ get current logged user
    """
    body = {
        'username': current_user
    }
    return body, 200


arg_signin = {
    'username': fields.Str(required=True),
    'password': fields.Str(required=True)
}


@blueprint.route('', methods=['POST'])
def signin():
    """ filter
    """
    arg = parser.parse(arg_signin, request, location='json')
    query = User.by_username(clear_str(arg['username'], upper=True))
    http_status = 404 if query is None else 200
    body = {'message': 'Usuario y password incorrecto'}
    if http_status == 200:
        if bcrypt_verify(arg['password'], query.get_passwd):
            if query.active:
                token = bearer_encode(query.cid, query.username)
                if token is None:
                    body['message'] = 'Ocurrio un error'
                else:
                    body = {
                        'token': token.decode(),
                        'username': query.username
                    }
            else:
                body['message'], http_status = 'Usuario inactivo', 403
        else:
            body['message'], http_status = 'Usuario y password incorrecto', 403
    else:
        http_status = 403
    return body, http_status


arg_passwd = {
    'password': fields.Str(required=True),
    'password_password': fields.Str(required=True)
}


@blueprint.route('passwd', methods=['PUT'])
@auth
def passwd(cid, current_user):
    """ change passwd
    """
    arg = parser.parse(arg_passwd, request, location='json')
    query = User.by_username(current_user)
    http_status = 404 if query is None else 200
    body = {'message': 'Las contraseñas no coenciden'}
    if http_status == 200:
        if safe_str_cmp(arg['password'], arg['password_password']):
            query.set_passwd(bcrypt_gen(arg['password']))
            query.save()
            body['message'] = 'Ok'
        else:
            http_status = 422
    return body, http_status


arg_signup = {
    'razon_social': fields.Str(required=True),
    'email': fields.Str(required=True),
    'telefono': fields.Str(required=True),
    'ruc': fields.Str(required=True),
    'username': fields.Str(required=True),
    'password': fields.Str(required=True)
}


@blueprint.route('signup', methods=['POST'])
def signup():
    """ create a configs and user
    """
    arg = parser.parse(arg_signup, request, location='json')
    query = User.by_username(arg['username'])
    http_status = 200 if query is None else 404
    body = {'message': 'Username existe. Elija otro'}
    if http_status == 200:
        config = Config()
        config.razon_social = clear_str(arg['razon_social'])
        config.nombre_comercial = clear_str(arg['razon_social'])
        config.email = clear_str(arg['email'], lower=True)
        config.email_sender = config.email
        config.telefono = clear_str(arg['telefono'])
        config.ruc = clear_str(arg['ruc'])
        config.referral = random_key(8)
        config.active = False
        config.enabled = False
        config.new()

        user = User(config.id, arg['username'])
        user.nombre = clear_str(arg['username'], upper=True)
        user.email = clear_str(arg['email'], upper=True)
        user.is_admin = True
        user.active = True
        user.set_passwd(bcrypt_gen(arg['password']))
        user.new()
        body = {'message': 'created'}

    return body, http_status
