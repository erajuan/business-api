""" empresas.py
"""
from datetime import datetime
from flask import Blueprint, request, jsonify
from webargs import fields
from webargs.flaskparser import parser

from app.tools import clear_str
from app.models.core import Empresa, Contacto
from app.api import auth

blueprint = Blueprint('empresas', __name__)


@blueprint.route('', methods=['GET'])
@auth
def find_all(cid, current_user):
    """ filter
    """
    body = list(map(lambda o: o.to_dict(), Empresa.all(cid)))
    return jsonify(body), 200


arg_req = {
    'nombre': fields.Str(required=True),
    'rubro': fields.Str(required=True),
    'relacion': fields.Int(required=True),
    'documento_tipo': fields.Str(allow_none=True),
    'documento_numero': fields.Str(allow_none=True),
    'email': fields.Str(allow_none=True),
    'telefono': fields.Str(allow_none=True),
    'direccion': fields.Str(allow_none=True),
    'ciudad': fields.Str(allow_none=True),
    'observacion': fields.Str(allow_none=True)
}


@blueprint.route('', methods=['POST'])
@auth
def create(cid, current_user):
    """ create
    """
    arg = parser.parse(arg_req, request, location='json')
    query = Empresa(cid)
    query.nombre = arg['nombre'].strip()
    query.rubro = arg['rubro'].strip()
    query.relacion = arg['relacion']
    query.documento_tipo = clear_str(arg['documento_tipo'])
    query.documento_numero = clear_str(arg['documento_numero'])
    query.email = clear_str(arg['email'])
    query.telefono = clear_str(arg['telefono'])
    query.direccion = clear_str(arg['direccion'])
    query.ciudad = clear_str(arg['ciudad'])
    query.observacion = clear_str(arg['observacion'])
    query.active = True
    query.new()
    return "", 201


@blueprint.route('<int:pk>', methods=['GET'])
@auth
def find_one(cid, current_user, pk):
    """ get one
    """
    query = Empresa.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not Found'}
    if http_status == 200:
        body = query.to_dict()
        contactos = Contacto.all(cid, pk)
        body['contactos'] = list(map(lambda o: o.to_dict(), contactos))
    return body, http_status


@blueprint.route('<int:pk>', methods=['PUT'])
@auth
def update(cid, current_user, pk):
    """ update
    """
    arg = parser.parse(arg_req, request, location='json')
    query = Empresa.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not Found'}
    if http_status == 200:
        query.nombre = arg['nombre'].strip()
        query.rubro = arg['rubro'].strip()
        query.relacion = arg['relacion']
        query.documento_tipo = clear_str(arg['documento_tipo'])
        query.documento_numero = clear_str(arg['documento_numero'])
        query.email = clear_str(arg['email'])
        query.telefono = clear_str(arg['telefono'])
        query.direccion = clear_str(arg['direccion'])
        query.ciudad = clear_str(arg['ciudad'])
        query.observacion = clear_str(arg['observacion'])
        query.active = True
        query.save()
        http_status = 204
        body = {'message': 'Updated'}
    return body, http_status


@blueprint.route('<int:pk>', methods=['DELETE'])
@auth
def delete(cid, current_user, pk):
    """ update
    """
    query = Empresa.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not found'}
    if http_status == 200:
        query.active = False
        query.save()
        http_status = 204
        body = {'message': 'Deleted'}
    return body, http_status
# Contactos


@blueprint.route('<int:empresa_id>/contactos', methods=['GET'])
@auth
def contactos_all(cid, current_user, empresa_id):
    """ filter
    """
    body = list(map(lambda o: o.to_dict(), Contacto.all(cid, empresa_id)))
    return jsonify(body), 200


arg_req_c = {
    'titulo': fields.Str(allow_none=True),
    'nombre': fields.Str(required=True),
    'email': fields.Str(allow_none=True),
    'telefono': fields.Str(allow_none=True),
    'direccion': fields.Str(allow_none=True),
    'ciudad': fields.Str(allow_none=True),
    'documento_identidad': fields.Str(allow_none=True),
    'cargo': fields.Str(allow_none=True),
    'observacion': fields.Str(allow_none=True)
}


@blueprint.route('<int:empresa_id>/contactos', methods=['POST'])
@auth
def contacto_create(cid, current_user, empresa_id):
    """ create
    """
    arg = parser.parse(arg_req_c, request, location='json')
    query = Contacto(cid, empresa_id)
    query.titulo = arg['titulo'].strip()
    query.nombre = arg['nombre'].strip().upper()
    query.email = clear_str(arg['email'], lower=True)
    query.telefono = clear_str(arg['telefono'])
    query.direccion = clear_str(arg['direccion'])
    query.ciudad = clear_str(arg['ciudad'])
    query.documento_identidad = clear_str(arg['documento_identidad'])
    query.cargo = clear_str(arg['cargo'])
    query.observacion = clear_str(arg['observacion'])
    query.active = True
    query.new()
    return {}, 201


@blueprint.route('<int:empresa_id>/contactos/<int:pk>', methods=['GET'])
@auth
def contacto_one(cid, current_user, empresa_id, pk):
    """ get one
    """
    query = Contacto.get(cid, empresa_id, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not Found'}
    if http_status == 200:
        body = query.to_dict()
    return body, http_status


@blueprint.route('<int:empresa_id>/contactos/<int:pk>', methods=['PUT'])
@auth
def contacto_update(cid, current_user, empresa_id, pk):
    """ update
    """
    arg = parser.parse(arg_req_c, request, location='json')
    query = Contacto.get(cid, empresa_id, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not Found'}
    if http_status == 200:
        query.titulo = arg['titulo'].strip()
        query.nombre = arg['nombre'].strip().upper()
        query.email = clear_str(arg['email'], lower=True)
        query.telefono = clear_str(arg['telefono'])
        query.direccion = clear_str(arg['direccion'])
        query.ciudad = clear_str(arg['ciudad'])
        query.documento_identidad = clear_str(arg['documento_identidad'])
        query.cargo = clear_str(arg['cargo'])
        query.observacion = clear_str(arg['observacion'])
        query.active = True
        query.save()
        http_status = 204
        body = {'message': 'Updated'}
    return body, http_status


@blueprint.route('<int:empresa_id>/contactos/<int:pk>', methods=['DELETE'])
@auth
def contacto_delete(cid, current_user, empresa_id, pk):
    """ update
    """
    query = Contacto.get(cid, empresa_id, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not found'}
    if http_status == 200:
        query.active = False
        query.save()
        http_status = 204
        body = {'message': 'Deleted'}
    return body, http_status
