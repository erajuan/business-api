""" nodes.py
"""
from flask import Blueprint
blueprint = Blueprint('nodes', __name__)

@blueprint.route("/")
def home():
    """ Initial page
    """
    return {'message': 'Welcome to API'}

@blueprint.app_errorhandler(422)
@blueprint.app_errorhandler(400)
def handle_errors(error):
    """ handle_errors
    """
    headers = error.data.get('headers', None)
    messages = error.data.get('messages', ['Invalid request.'])
    response = response = {'errors': messages}, error.code
    if headers:
        response = {'errors': messages}, error.code, headers        
    return response

@blueprint.app_errorhandler(500)
def handle_server_errors(error):
    """ handle_errors
    """
    return {'errors': 'Internal Server Error'}, error.code

@blueprint.app_errorhandler(404)
def handle_error_not_found(error):
    """ handle_errors
    """
    return {'errors': 'Not found try again'}, error.code
