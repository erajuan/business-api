""" ordenes.py
"""
from datetime import datetime
from flask import Blueprint, request, jsonify, render_template
from webargs import fields
from webargs.flaskparser import parser

from app.tools import clear_str
from app.tasks import create_email_data, send_mail
from app.models import Config
from app.models.cash import Orden, OrdenItem
from app.api import auth

blueprint = Blueprint('ordenes', __name__)


@blueprint.route('', methods=['GET'])
@auth
def find_all(cid, current_user):
    """ filter
    """
    body = list(map(lambda o: o.to_dict(), Orden.all(cid)))
    return jsonify(body), 200


arg_item = {
    'id': fields.Int(required=True),
    'servicio_id': fields.Int(required=True),
    'descripcion': fields.Str(required=True),
    'cantidad': fields.Float(required=True),
    'precio_unitario': fields.Float(required=True),
    'active': fields.Boolean(required=True)
}
arg_req = {
    'empresa_id': fields.Int(required=True),
    'moneda': fields.Str(required=True),
    'descuento': fields.Float(required=True),
    'es_renovacion': fields.Boolean(required=True),
    'incluye_igv': fields.Boolean(allow_none=True),
    'estado': fields.Integer(required=True),
    'fecha_vencimiento': fields.Date(required=True),
    'observacion': fields.Str(allow_none=True),
    'items': fields.List(fields.Nested(arg_item), required=True)
}


@blueprint.route('', methods=['POST'])
@auth
def create(cid, current_user):
    """ create
    """
    arg = parser.parse(arg_req, request, location='json')
    sub_total = 0
    for i in arg['items']:
        sub_total += i['cantidad'] * i['precio_unitario']

    query = Orden(cid)
    query.empresa_id = arg['empresa_id']
    query.moneda = arg['moneda'].strip()
    query.sub_total = sub_total
    query.descuento = arg['descuento']
    query.es_renovacion = arg['es_renovacion']
    query.incluye_igv = arg['incluye_igv']
    query.estado = arg['estado']
    query.fecha_vencimiento = arg['fecha_vencimiento']
    query.observacion = clear_str(arg['observacion'])
    query.active = True
    query.new(current_user)
    for i in arg['items']:
        item = OrdenItem(cid, query.id)
        item.servicio_id = i['servicio_id']
        item.descripcion = clear_str(i['descripcion'])
        item.cantidad = i['cantidad']
        item.precio_unitario = i['precio_unitario']
        item.active = True
        item.add(False)
    OrdenItem.commit()
    return "", 201


@blueprint.route('<int:pk>', methods=['GET'])
@auth
def find_one(cid, current_user, pk):
    """ get one
    """
    query = Orden.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not Found'}
    if http_status == 200:
        body = query.to_dict()
        items = list(map(lambda i: i.to_dict(), OrdenItem.all(cid, pk)))
        body['items'] = items
    return body, http_status


@ blueprint.route('<int:pk>', methods=['PUT'])
@ auth
def update(cid, current_user, pk):
    """ update
    """
    arg = parser.parse(arg_req, request, location='json')
    query = Orden.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not Found'}
    if http_status == 200:
        sub_total = 0
        for i in arg['items']:
            if i['active']:
                sub_total += i['cantidad'] * i['precio_unitario']
        query.sub_total = sub_total
        query.empresa_id = arg['empresa_id']
        query.moneda = arg['moneda'].strip()
        query.descuento = arg['descuento']
        query.es_renovacion = arg['es_renovacion']
        query.incluye_igv = arg['incluye_igv']
        query.estado = arg['estado']
        query.fecha_vencimiento = arg['fecha_vencimiento']
        query.observacion = clear_str(arg['observacion'])
        query.active = True
        query.save()
        # insert items
        for i in arg['items']:
            if i['id'] > 0:
                item = OrdenItem.get(pk, i['id'])
                item.servicio_id = i['servicio_id']
                item.descripcion = clear_str(i['descripcion'])
                item.cantidad = i['cantidad']
                item.precio_unitario = i['precio_unitario']
                item.active = i['active']
                item.add(False)
            else:
                item = OrdenItem(cid, pk)
                item.servicio_id = i['servicio_id']
                item.descripcion = clear_str(i['descripcion'])
                item.cantidad = i['cantidad']
                item.precio_unitario = i['precio_unitario']
                item.active = True
                item.add(False)
        OrdenItem.commit()
        http_status = 204
        body = {'message': 'Updated'}
    return body, http_status


@ blueprint.route('<int:pk>', methods=['DELETE'])
@ auth
def delete(cid, current_user, pk):
    """ update
    """
    query = Orden.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not found'}
    if http_status == 200:
        query.estado = 0
        query.save()
        http_status = 204
        body = {'message': 'Deleted'}
    return body, http_status


@ blueprint.route('<int:pk>/send', methods=['POST'])
@ auth
def send(cid, current_user, pk):
    """ send email
    """
    query = Orden.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not found'}
    if http_status == 200 and query.empresa.email is None:
        http_status = 400
        body['message'] = 'Cliente no tiene E-mail'

    if http_status == 200:
        items = list(map(lambda i: i.to_dict(), OrdenItem.all(cid, pk)))
        config = Config.get(cid)
        data = {
            'orden': query.to_dict(),
            'config': config.to_dict(),
            'subject': 'Orden de pago OP-%i' % query.id
        }
        data['orden']['items'] = items
        # renderizar los correos
        email_body = render_template('mails/orden.txt', **data)
        email_html = render_template('mails/orden.html', **data)
        # Create email
        email_data = create_email_data(config, query.empresa.email)
        email_data['subject'] = data['subject']
        email_data['body'] = email_body
        email_data['html'] = email_html
        # enviar email
        send_mail.delay(email_data)
        # responder peticion
        body = {'message': 'Enviado'}
    return body, http_status
