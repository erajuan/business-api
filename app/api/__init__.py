""" api
"""
import functools
import jwt
from datetime import datetime, timedelta
from flask import request, jsonify
from config import Config


def bearer_encode(cid, user, salt='P3me5'):
    """
    Generates the access token to be used as the Authorization header
    """
    try:
        # set up a payload with an expiration time
        payload = {
            'exp': datetime.utcnow() + timedelta(minutes=240),
            'iat': datetime.utcnow(),
            'nbf': datetime.utcnow(),
            'sub': user,
            'cid': cid,
            'iss': 'pymes.com.pe'
        }
        # create the byte Str token using the payload and the SECRET key
        token = jwt.encode(
            payload,
            '{}{}'.format(Config.SECRET_KEY, salt),
            algorithm='HS256'
        )
        return token

    except Exception as exception:
        # return an error in Str format if an exception occurs
        return None


def bearer_decode(token, salt='P3me5'):
    """
    Decode the access token from the Authorization header.
    """
    try:
        payload = jwt.decode(
            token,
            '{}{}'.format(Config.SECRET_KEY, salt),
            algorithms=['HS256']
        )
        return payload['sub'], payload['cid'], 200
    except jwt.ExpiredSignatureError:
        return {'message': 'Expired token. Please log in'}, 0, 401
    except jwt.InvalidTokenError:
        return {'message': 'Invalid token. Please register or login'}, 0, 403


def auth(func):
    """ Verify if is auth with cid, username
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        access_token = request.headers.get('Authorization')
        body, http_status = {'message': 'Please login!'}, 403
        if access_token is None:
            body, http_status = {'message': 'Please you need signin'}, 403
        else:
            if access_token.startswith('Bearer '):
                token = access_token.split(' ')
                authorized_user, cid, http_status = bearer_decode(token[1])
                if http_status == 200:
                    body, http_status = func(cid, authorized_user, *args, **kwargs)
                else:
                    body = authorized_user
        return body, http_status
    return wrapper

