""" users.py
"""
from app.extensions import db
from datetime import datetime
from flask import Blueprint, request, jsonify
from webargs import fields
from webargs.flaskparser import parser

from app.tools import clear_str
from app.tools.bcrypt import bcrypt_gen
from app.models import User
from app.api import auth

blueprint = Blueprint('users', __name__)


@blueprint.route('', methods=['GET'])
@auth
def find_all(cid, current_user):
    """ filter
    """
    body = list(map(lambda o: o.to_dict(), User.all(cid)))
    return jsonify(body), 200


arg_post = {
    'username': fields.Str(required=True),
    'password': fields.Str(required=True),
    'nombre': fields.Str(required=True),
    'email': fields.Str(required=True),
    'is_admin': fields.Boolean(required=True)
}


@blueprint.route('', methods=['POST'])
@auth
def create(cid, current_user):
    """ create
    """
    arg = parser.parse(arg_post, request, location='json')
    query = User(cid, arg['username'])
    not_exists = query.not_exists()
    body = {'message': 'Username en uso, elija otro username'}
    http_status = 404
    if not_exists:
        query.nombre = arg['nombre']
        query.email = arg['email']
        query.set_passwd(bcrypt_gen(arg['password']))
        query.is_admin = arg['is_admin']
        query.active = True
        query.new()
        http_status = 201
        body = ""
    print(body, http_status, not_exists)
    return body, http_status


@blueprint.route('<int:pk>', methods=['GET'])
@auth
def find_one(cid, current_user, pk):
    """ get one
    """
    query = User.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not Found'}
    if http_status == 200:
        body = query.to_dict()
    return body, http_status


arg_put = {
    'nombre': fields.Str(required=True),
    'email': fields.Str(required=True),
    'is_admin': fields.Boolean(required=True)
}


@blueprint.route('<int:pk>', methods=['PUT'])
@auth
def update(cid, current_user, pk):
    """ update
    """
    arg = parser.parse(arg_put, request, location='json')
    query = User.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not Found'}
    if http_status == 200:
        query.nombre = arg['nombre']
        query.email = arg['email']
        query.is_admin = arg['is_admin']
        query.active = True
        query.save()
        http_status = 204
        body = {'message': 'Updated'}
    return body, http_status


@blueprint.route('<int:pk>', methods=['DELETE'])
@auth
def delete(cid, current_user, pk):
    """ update
    """
    query = User.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not found'}
    if http_status == 200:
        query.active = False
        query.save()
        http_status = 204
        body = {'message': 'Deleted'}
    return body, http_status


arg_passwd = {
    'password': fields.Str(required=True)
}


@blueprint.route('<int:pk>/passwd', methods=['PUT'])
@auth
def passwd(cid, current_user, pk):
    """ change passwd
    """
    arg = parser.parse(arg_passwd, request)
    query = User.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Usuario no existe'}
    if http_status == 200:
        query.set_passwd(bcrypt_gen(arg['password']))
        query.save()
        body = ""
    return body, http_status
