""" empleados.py
"""
from app.extensions import db
from datetime import datetime
from flask import Blueprint, request, jsonify
from webargs import fields
from webargs.flaskparser import parser

from app.tools import clear_str
from app.models.core import Empleado
from app.api import auth

blueprint = Blueprint('empleados', __name__)


@blueprint.route('', methods=['GET'])
@auth
def find_all(cid, current_user):
    """ filter
    """
    body = list(map(lambda o: o.to_dict(), Empleado.all(cid)))
    return jsonify(body), 200


arg_req = {
    'nombre': fields.Str(required=True),
    'apellido': fields.Str(allow_none=True),
    'documento_tipo': fields.Str(required=True),
    'documento_numero': fields.Str(required=True),
    'sexo': fields.Str(required=True),
    'fecha_nacimiento': fields.Date(required=True),
    'email_asignado': fields.Str(allow_none=True),
    'email': fields.Str(allow_none=True),
    'telefono': fields.Str(allow_none=True),
    'direccion': fields.Str(allow_none=True),
    'observacion': fields.Str(allow_none=True),
    'cargo': fields.Str(required=True),
    'fecha_ingreso': fields.Date(required=True),
    'sueldo': fields.Float(required=True),
    'condicion': fields.Str(required=True),
    'sede': fields.Str(allow_none=True)
}


@blueprint.route('', methods=['POST'])
@auth
def create(cid, current_user):
    """ create
    """
    arg = parser.parse(arg_req, request, location='json')
    query = Empleado(cid)
    query.nombre = arg['nombre'].strip().upper()
    query.apellido = arg['apellido'].strip().upper()
    query.documento_tipo = arg['documento_tipo'].strip()
    query.documento_numero = arg['documento_numero'].strip()
    query.sexo = arg['sexo']
    query.fecha_nacimiento = arg['fecha_nacimiento']
    query.email_asignado = clear_str(arg['email_asignado'])
    query.email = clear_str(arg['email'])
    query.telefono = clear_str(arg['telefono'])
    query.direccion = clear_str(arg['direccion'])
    query.observacion = clear_str(arg['observacion'])
    query.cargo = arg['cargo'].strip()
    query.fecha_ingreso = arg['fecha_ingreso']
    query.sueldo = arg['sueldo']
    query.condicion = arg['condicion']
    query.sede = clear_str(arg['sede'])
    query.active = True
    query.new()
    return {}, 201


@blueprint.route('<int:pk>', methods=['GET'])
@auth
def find_one(cid, current_user, pk):
    """ get one
    """
    query = Empleado.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not Found'}
    if http_status == 200:
        body = query.to_dict()
    return body, http_status


@blueprint.route('<int:pk>', methods=['PUT'])
@auth
def update(cid, current_user, pk):
    """ update
    """
    arg = parser.parse(arg_req, request, location='json')
    query = Empleado.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not Found'}
    if http_status == 200:
        query.nombre = arg['nombre'].strip().upper()
        query.apellido = arg['apellido'].strip().upper()
        query.documento_tipo = arg['documento_tipo'].strip()
        query.documento_numero = arg['documento_numero'].strip()
        query.sexo = arg['sexo']
        query.fecha_nacimiento = arg['fecha_nacimiento']
        query.email_asignado = clear_str(arg['email_asignado'])
        query.email = clear_str(arg['email'])
        query.telefono = clear_str(arg['telefono'])
        query.direccion = clear_str(arg['direccion'])
        query.observacion = clear_str(arg['observacion'])
        query.cargo = arg['cargo'].strip()
        query.fecha_ingreso = arg['fecha_ingreso']
        query.sueldo = arg['sueldo']
        query.condicion = arg['condicion']
        query.sede = clear_str(arg['sede'])
        query.active = True
        query.save()
        http_status = 204
    body = {'message': 'Updated'}
    return body, http_status


@blueprint.route('<int:pk>', methods=['DELETE'])
@auth
def delete(cid, current_user, pk):
    """ update
    """
    query = Empleado.get(cid, pk)
    http_status = 404 if query is None else 200
    body = {'message': 'Not found'}
    if http_status == 200:
        query.active = False
        query.save()
        http_status = 204
        body = {'message': 'Deleted'}
    return body, http_status
