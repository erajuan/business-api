from flask import Flask
from flask_migrate import Migrate
from flask_cors import CORS

from app.flask_celery import init_celery
from app.extensions import db, mail, celery
from config import Config

migrate = Migrate()


def create_app():
    app = Flask(__name__, static_folder=None)
    app.config.from_object(Config)
    db.init_app(app)
    mail.init_app(app)
    migrate.init_app(app, db)
    CORS(app, resources={r'/*': {'origins': 'http://localhost:4200', }})
    init_celery(celery, app)

    from app.api.nodes import blueprint as bp_nodes
    app.register_blueprint(bp_nodes)

    from app.api.cuentas import blueprint as bp_cuentas
    app.register_blueprint(bp_cuentas, url_prefix='/cuentas')

    from app.api.users import blueprint as bp_users
    app.register_blueprint(bp_users, url_prefix='/users')

    from app.api.servicios import blueprint as bp_servicios
    app.register_blueprint(bp_servicios, url_prefix='/servicios')

    from app.api.empresas import blueprint as bp_empresas
    app.register_blueprint(bp_empresas, url_prefix="/empresas")

    from app.api.empleados import blueprint as bp_empleados
    app.register_blueprint(bp_empleados, url_prefix="/empleados")

    from app.api.suscripciones import blueprint as bp_suscripciones
    app.register_blueprint(bp_suscripciones, url_prefix="/suscripciones")

    from app.api.ordenes import blueprint as bp_ordenes
    app.register_blueprint(bp_ordenes, url_prefix="/ordenes-pagos")

    from app.api.account import blueprint as bp_account
    app.register_blueprint(bp_account, url_prefix="/auth")

    @app.template_filter()
    def date_peru(date):
        if date is None:
            return ''
        else:
            parts = date[:10].split('-')
            return "{}/{}/{}".format(parts[2], parts[1], parts[0])
    # FLASK APP
    return app
